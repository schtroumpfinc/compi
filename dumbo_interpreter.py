# -*- coding: utf-8 -*-
from syntax import interpret

if __name__=='__main__':
	import sys
	i = 1
	programs = []
	lastindex = len(sys.argv)-1
	while i < lastindex:
		programs.append(file(sys.argv[i]).read())
		i += 1
	result = interpret(programs)
	f = open(sys.argv[lastindex] , "w")
	f.write(result)
	f.close()
	print "Terminé"
