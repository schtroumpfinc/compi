# -*- coding: utf-8 -*-
import ply.yacc as yacc
from lex import tokens
from ast import *

data = {}
# rajouter une liste chaînée/pile pour les dictionnaires 
# pour gérer les for imbriqués ?

def p_program_txt(p):
	'''program : TXT'''
	p[0] = ProgramNode(TxtNode(p[1]))
def p_program_dumbo(p):
	'''program : dumboBloc'''
	p[0] = ProgramNode(p[1])

def p_program_txtAndProg(p):
	'''program : TXT program'''
	p[0] = ProgramNode(TxtNode(p[1]) , p[2])
def p_program_dumboAndProg(p):
	'''program : dumboBloc program'''
	p[0] = ProgramNode(p[1] , p[2])
	
def p_dumboBloc(p):
	'''dumboBloc : CODEBEGIN expressionList CODEEND'''
	p[0] = DumboNode(p[2])
	
def p_emptyDumboBloc(p):
	'''dumboBloc : CODEBEGIN CODEEND'''
	p[0]= TxtNode('')
	
def p_expressionList_recursive(p):
	'''expressionList : expression ENDEXP expressionList'''
	p[0] = [p[1]] + p[3]#le noeud sera créé lorsque la liste sera complète

def p_expressionList_expression(p):
	'''expressionList : expression ENDEXP'''
	p[0] = [p[1]]#le noeud sera créé lorsque la liste sera complète

def p_expression_print(p):
	'''expression : PRINT stringExpression'''
				  # | PRINT number
				  # | PRINT boolean'''
				  # | PRINT variable''' r/r conflict
	p[0] = PrintNode(p[2])

def p_expression_forlist(p):
	'''expression : FOR VARIABLE IN stringList DO expressionList ENDFOR'''
	p[0] = ForNode(p[2], TxtNode(p[4]), DumboNode(p[6]))
def p_expression_forvar(p):
	'''expression :  FOR VARIABLE IN variable DO expressionList ENDFOR'''
	p[0] = ForNode(p[2], p[4], DumboNode(p[6]))
	
def p_expression_assign(p):
	'''expression : VARIABLE EQUALS stringExpression
				  | VARIABLE EQUALS stringList
				  | VARIABLE EQUALS number
				  | VARIABLE EQUALS boolean'''
				  # | VARIABLE EQUALS variable''' r/r conflict
	p[0] = AssertNode(p[1] , p[3])
	
def p_stringExpression_string(p):
	'''stringExpression : STRING'''
	p[0] = TxtNode(p[1])	
def p_stringExpression_var(p):
	'''stringExpression : variable'''
	p[0] = PrintNode(p[1])
		
def p_stringExpression_recursive(p):
	'''stringExpression : stringExpression STRINGEXPSEPARATOR stringExpression'''
	p[0] = StringConcatNode(p[1],p[3])
	
def p_stringList_interior(p):
	'''stringList : STRINGLISTOPEN stringListInterior STRINGLISTCLOSE'''
	p[0] = ListNode(p[2])
	
def p_stringListInterior_stringListInterior(p):	
	'''stringListInterior : STRING VIRGULE stringListInterior'''
	p[0]=[p[1]]+p[3]#le noeud sera créé lorsque la liste sera complète
	
def p_stringListInterior_string(p):
	'''stringListInterior : STRING'''
	p[0]=[p[1]]#le noeud sera créé lorsque la liste sera complète
	
def p_expression_ifBloc(p):
	'''expression : IF boolean DO expressionList ENDIF
				  | IF variable DO expressionList ENDIF'''
	p[0]=IfNode(p[2],p[4])
		

def p_number_integer(p):
	'''number : INTEGER'''
	p[0]=TxtNode(p[1])
	
def p_number_operation(p):
	'''number : number OPERATOR number
			  | number OPERATOR variable
			  | variable OPERATOR number
			  | variable OPERATOR variable'''
	p[0]=OperatorNode(p[1],p[2],p[3])
	
def p_boolean_static(p):
	'''boolean : TRUE
			   | FALSE'''
	p[0]=TxtNode(p[1])
	
def p_boolean_numbersComparaison(p):	
	'''boolean : number COMPARATOR number
			   | number COMPARATOR variable
			   | variable COMPARATOR number
			   | variable COMPARATOR variable'''
	p[0]=OperatorNode(p[1],p[2],p[3])
	
def p_boolean_operation(p):	
	'''boolean : boolean BOOLOP boolean
			   | variable BOOLOP boolean
			   | boolean BOOLOP variable
			   | variable BOOLOP variable'''
	p[0]=OperatorNode(p[1],p[2],p[3])
	
def p_variable(p):
	'''variable : VARIABLE'''
	p[0]=VariableNode(p[1])
	
def p_error(p):
	print("Syntax error in line {}".format(p.lineno))
	print(p)
	# yacc.error()

precedence = (
	("left", "STRINGEXPSEPARATOR"),#concatenation de la gauche vers la droite
	("left", "BOOLOP"),
	("left", "OPERATOR"),
)

yacc.yacc(outputdir='generated')


if __name__=='__main__':
	import sys
	# TODO penser à appliquer la forme "dumbo_interpreter.py data template output"
	# On assemble tous les fichiers donnés en paramètre
	i = 1
	program = ""
	while i < len(sys.argv):
		program += file(sys.argv[i]).read()
		i += 1
	# calcul du résultat
	result = yacc.parse(program)
	# écriture du résultat dans le fichier de sortie
	# fichier = open(sys.argv[3], "w")
	# fichier.write("result")
	# fichier.close()
	# affichage console
	print(result.value(data))
	print(data)

def interpret(programs):
	table = {}
	produit = ""
	for script in programs:
		ast = yacc.parse(script)
		produit += ast.value(table)
	return produit
