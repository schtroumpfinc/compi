# -*- coding: utf-8 -*-
import ply.lex as lex

static = {
'for' : 'FOR',
'in' : 'IN',
'do' : 'DO',
'endfor' : 'ENDFOR',
':=' : 'EQUALS',
';' : 'ENDEXP',
'print' : 'PRINT',
'if' : 'IF',
'endif' : 'ENDIF',
'true' : 'TRUE',
'false' : 'FALSE',
',' : 'VIRGULE',
# '' : '',
}

tokens = [
	'CODEBEGIN',
	'CODEEND',
	'TXT',
	'VARIABLE',
	'STRINGLISTOPEN',
	'STRINGLISTCLOSE',
	'STRINGEXPSEPARATOR',
	'STRING',
	'INTEGER',
	'OPERATOR',
	'COMPARATOR',
	'BOOLOP',
	# '',
] + list(static.values())

states=(
	('inCode','exclusive'),
)

t_TXT=r'[a-zA-Z0-9;&<>"_\-\./\\\s\p:,]+'

def t_CODEBEGIN(t):
	r'\{\{'
	t.lexer.begin('inCode')
	return t

def t_inCode_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)	
	
def t_inCode_BLANK(t):
	r'\s+'
	# return t

def t_inCode_CODEEND(t):
	r'\}\}'
	t.lexer.begin('INITIAL')
	return t

# http://www.dabeaz.com/ply/ply.html#ply_nn6 sauce
def t_inCode_STATIC(t):
    r'for|in|do|endfor|:=|;|print|if|endif|,|true|false'
    t.type = static.get(t.value)
    return t
	
def t_inCode_BOOLOP(t):
	r'or|and'
	return t
	
def t_inCode_OPERATOR(t):
	r'\+|-|\*|/'
	return t

def t_inCode_COMPARATOR(t):
	r'>|<|=|!='
	return t
	
def t_inCode_INTEGER(t):
	r'\d+'
	t.value =int(t.value)
	return t	
	
def t_inCode_VARIABLE(t):
	r'[a-zA-Z0-9_]+'
	return t
	
def t_inCode_STRINGEXPSEPARATOR(t):
	r'\.'
	return t

def t_inCode_STRINGLISTOPEN(t):
	r'\('
	return t

def t_inCode_STRINGLISTCLOSE(t):
	r'\)'
	return t
	
def t_inCode_STRING(t):
	r'\'[a-zA-Z0-9;&<>_\-\./\\\n\p:,= "]+\''
	t.value=t.value[1:len(t.value)-1]
	return t

# def t_inCode_(t):
	
def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1) 
	
def t_inCode_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1) 
	
lexer=lex.lex()	

def printlexs(filename, printvalue=False):
	"""
	Affiche les lexèmes. Fonctions utiles pour les tests unitaires
	"""
	datafile = open(filename,"r")
	lexer.input(datafile.read()) #lire data.dumbo
	if printvalue:
		for token in lexer:
			print token.type+":"+token.value
	else:
		for token in lexer:
			print token.type

# if __name__== "__main__":
	# data ={} #dictionnaire de donnees
	# import sys
	# lexer=lex.lex()
	# datafile=open(sys.argv[1],"r")
	# lexer.input(datafile.read()) #lire data.dumbo
	# temp = ""
	# for token in lexer:
		# print("line %d : %s (%s) " %(token.lineno,token.type,token.value))
		# if token.type == 'VARIABLE':
			# temp = token.value
		# elif token.type == 'STRING':
			# data[temp]=token.value
		# elif token.type == 'STRINGLIST':
			# data[temp]=token.value[1:len(token.value)-1].split(',')
	# print(data)
