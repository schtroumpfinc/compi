# -*- coding: utf-8 -*-
'''
Tous les noeuds qui peuvent composer notre Abstract Syntax Tree
@author Barat Kevin et Bury Jason
'''
class Node: #classe "abstraite"
	def value(self, data):
		'''
		Évalue le noeud courant (et ses fils avec) pour retourner l'évaluation complète de cet expression.
		param data: dictionnaire
		Retourne une chaîne de caractère
		'''
		raise NotImplementedError

class ProgramNode:
	def __init__(self, node1, node2=None):
		self.n1 = node1
		self.n2 = node2
	def value(self, data):
		result = self.n1.value(data)
		if self.n2 != None:
			result += self.n2.value(data)
		return result

class DumboNode:
	def __init__(self, explist):
		self.l = explist
	def value(self, data):
		result = ""
		for expression in self.l:
			result += expression.value(data)
		return result

class PrintNode:
	def __init__(self, message):
		self.m = message
	def value(self, data):
		'''Converti m en chaîne de caractère'''
		return str(self.m.value(data))

class ForNode:
	def __init__(self, variable, sequence, bloc):
		self.v = variable
		self.l = sequence
		self.prog = bloc
	def value(self, data):
		'''En remplaçant systématiquement variable par sequence[i] dans data, execute bloc en boucle'''
		previous = data.get(self.v) #None si non existant
		result = ""
		for item in self.l.value(data):
			data[self.v] = item
			result += self.prog.value(data)
		if previous == None:
			data.pop(self.v)
		else:
			data[self.v] = previous
		return result

class StringConcatNode:
	def __init__(self, exp1, exp2):
		self.e1 = exp1
		self.e2 = exp2
	def value(self, data):
		'''Concatène exp1 et exp2'''
		return self.e1.value(data)+self.e2.value(data)

# Feuilles ---
class AssertNode:
	def __init__(self, variablename, value):
		self.name = variablename
		self.val = value
	def value(self, data):
		data[self.name] = self.val.value(data)
		return ""

class ListNode:
	def __init__(self, slist):
		self.l = slist
	def value(self, data):
		return self.l

class TxtNode: # pour un lexème TXT ou STRING ou NUMBER ou autre feuille ne nécessitant aucune évaluation
	def __init__(self, txt):
		self.s = txt
	def value(self, data):
		'''Évalue une chaîne de caractère'''
		return self.s

class VariableNode:
	def __init__(self, variable):
		self.var = variable
	def value(self, data):
		'''
		Cherche la valeur de la variable dans le dictionnaire data et la retourne
		Retourne None si la variable n'est pas dans le dictionnaire
		'''
		return data.get(self.var)

class OperatorNode(Node):
	def __init__(self, x, op, y):
		self.x=x
		self.y=y
		self.op=op
		self.operator = {
		'>' : lambda x, y: x>y,
		'<' : lambda x, y: x<y,
		'=' : lambda x, y: x==y,
		'!=' : lambda x, y: x!=y,
		'+' : lambda x, y: x+y,
		'-' : lambda x, y: x-y,
		'*' : lambda x, y: x*y,
		'/' : lambda x, y: x/y,
		'or' : lambda x, y: x or y,
		'and' : lambda x, y: x and y,
		}
	def value(self, data):
		'''
		Retourne le résultat de l'opération op sur x et y.
		'''
		return self.operator[self.op](self.x.value(data),self.y.value(data))	
		
class IfNode(Node):
	def __init__(self, boolean, children):
		self.boolean=boolean
		self.children=children
	def value(self,data):
		res=""
		bool=self.boolean.value(data)
		if bool=="true" or bool:
			for child in self.children:
				res+=child.value(data)
		return res
